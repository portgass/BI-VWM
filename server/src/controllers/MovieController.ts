import { Request, Response } from 'express'
import * as R from 'ramda'

import Movie from '../Movie'
import MovieService, { MovieData, AttributeMap } from '../services/MovieService'

interface MovieRequest {
	aggregate: string
	count: number
	attributes: string[]
	order: 'desc' | 'asc'
}

export interface MovieMap {
	[id: string]: Movie
}

export type MoviesFromRangeFunction = 
	(data: MovieData, attributes: string[], from: number, count: number)
		=> MovieMap
export type GetThresholdFunction = 
	(data: MovieData, attributes: string[], maxAttributes: AttributeMap, minValues: AttributeMap, from: number, type: string)
		=> number
export type MapAggregateFunction =
	(attributes: string[], maxAttributes: AttributeMap, minValues: AttributeMap, type: string)
		=> (movie: Movie) => Movie
export type ControllerFunction =
	(service: MovieService) => (req: Request, res: Response) => void

/**
 * Compares attributes of movie and returns value prepared to sort function
 * Can set descending order
 * 
 * @param {boolean} [descending=true] 
 * @param {Movie} a
 * @param {Movie} b 
 */
const sortByAggregate = (descending: boolean = true) => (a: Movie, b: Movie) => {
	if (a.aggregate === undefined || b.aggregate === undefined) { return 0 }

	if (descending) {
		return b.aggregate > a.aggregate ? 1 : b.aggregate === a.aggregate ? 0 : -1
	} else {
		return a.aggregate > b.aggregate ? 1 : a.aggregate === b.aggregate ? 0 : -1
	}
}

/**
 * Counts the aggreagate value from values by aggregate function type
 * 
 * @param {string} type 
 * @param {number[]} values 
 * @returns {number} 
 */
const aggregateValues = (type: string, values: number[]): number => {
	if (type === 'max') {
		return Math.max(...values)
	} else if (type === 'min') {
		return Math.min(...values)
	} else if (type === 'average') {
		return R.reduce<number, number>(R.add, 0, values) / values.length
	} else if (type === 'sum') {
		return R.reduce<number, number>(R.add, 0, values)
	} else {
		return values[0]
	}
}

/**
 * Gets all attribute values and assigns aggregate value to movie
 * If aggregate value is assigned, returns
 * 
 * @param {string[]} attributes 
 * @param {AttributeMap} maxAttributes 
 * @param {AttributeMap} minValues 
 * @param {string} type 
 * @param {Movie} movie 
 */
const mapAggregate: MapAggregateFunction =
	(attributes, maxAttributes, minValues, type) => (movie) => {
	if (movie.aggregate !== undefined) { return movie }

	const values: number[] = R.map(a => (movie[a] + minValues[a]) / maxAttributes[a], attributes)
	movie.aggregate = aggregateValues(type, values)
	return movie
}

/**
 * Does a parallel search on movies by attributes and returns count movies starying from index 
 * 
 * @param {MovieData} data 
 * @param {string[]} attributes 
 * @param {number} from 
 * @param {number} count 
 * @returns 
 */
const moviesFromRange: MoviesFromRangeFunction = (data, attributes, from, count) => {
	const attrCount = attributes.length
	let movies: MovieMap = {}
	
	for (let i = 0; i < count; i++) {
		const attribute = (from + i) % attrCount
		const index = Math.floor((from + i) / attrCount)
		const movie = data[attributes[attribute]][index]

		movies[movie.id] = Object.assign({}, movie)
	}

	return movies
}

/**
 * Gets the maximum aggregate value of unread movies
 * 
 * @param {MovieData} data 
 * @param {string[]} attributes 
 * @param {AttributeMap} maxAttributes 
 * @param {AttributeMap} minValues 
 * @param {number} from 
 * @param {string} type 
 * @returns 
 */
const computeThreshold: GetThresholdFunction =
	(data, attributes, maxAttributes, minValues, from, type) => {
	const attrCount = attributes.length
	let values: number[] = []

	for (let i = 0; i < attrCount; i++) {
		const attributeIndex = (from + i - attrCount) % attrCount
		const attribute = attributes[attributeIndex]
		const index = Math.floor((from + i - attrCount) / attrCount)
		const value = data[attribute][index][attribute] + minValues[attribute]

		values = R.append(value / maxAttributes[attribute], values)
	}

	return aggregateValues(type, values)
}

/**
 * Does a sequential iteration on all movies, counts their aggregate value and
 * list of the k best movies
 * 
 * @param {MovieService} service 
 * @param {Request} req 
 * @param {Response} res 
 */
export const getSequential: ControllerFunction = service => async (req, res) => {
	const data: MovieRequest = req.body
	const { aggregate, attributes, count, order } = data

	if (!service.loaded) {
		return res.sendStatus(404)
	}

	const iterationCount = service.movies.length
	let time = process.hrtime()

	let movies: Movie[] = service.movies
	movies = movies.map(m => Object.assign({}, m))
	movies = movies.map(mapAggregate(
		attributes,
		service.maxAttribute,
		service.minAttributeValue,
		aggregate
	))
	movies = movies.sort(sortByAggregate(order === 'desc'))
	movies = movies.slice(0, count)
	
	time = process.hrtime(time)
	res.json({
		movies,
		time: (time[0] * 1e9 + time[1]) / 1e6,
		count: iterationCount
	})
}

/**
 * Runs cycle over movies until best k movies based on aggregate value are found
 * Every iteration gets next unread movies, counts their aggregate value,
 * orders them, selects the best k, if k of last movie in list is better than
 * the max aggregate value of unread items, stops iterating and returns list of movies
 * 
 * @param {MovieService} service 
 * @param {Request} req 
 * @param {Response} res 
 */
export const getThreshold: ControllerFunction = service => async (req, res) => {
	const data: MovieRequest = req.body
	const { aggregate, attributes, count, order } = data

	if (!service.loaded) {
		return res.sendStatus(404)
	}

	let time = process.hrtime()
	
	const movies = order === 'desc' ?
		service.moviesByAttributes : service.moviesByAttributesReversed
	let topResults: MovieMap = {}
	let topResultsArr: Movie[] = []
	let unreadIndex = 0

	while (true) {
		topResults = R.merge(
			topResults,
			moviesFromRange(movies, attributes, unreadIndex, count)
		)
		topResultsArr = R.values(topResults)
		topResultsArr = R.map(
			mapAggregate(
				attributes,
				service.maxAttribute,
				service.minAttributeValue,
				aggregate
			),
			topResultsArr
		)
		topResultsArr = R.sort(sortByAggregate(order === 'desc'), topResultsArr)
		topResultsArr = R.slice(0, count, topResultsArr)

		unreadIndex += count

		if (unreadIndex > service.movies.length) {
			unreadIndex = service.movies.length
			break
		}

		const threshold = computeThreshold(
			movies,
			attributes,
			service.maxAttribute,
			service.minAttributeValue,
			unreadIndex,
			aggregate
		)
		const lastResult = topResultsArr[topResultsArr.length - 1]
		if (lastResult.aggregate && topResultsArr.length >= count) {
			if (order === 'desc' && threshold <= lastResult.aggregate) {
				break
			} else if (order === 'asc' && threshold >= lastResult.aggregate) {
				break
			}
		}
	}

	time = process.hrtime(time)
	res.json({
		movies: topResultsArr,
		time: (time[0] * 1e9 + time[1]) / 1e6,
		count: unreadIndex
	})
}