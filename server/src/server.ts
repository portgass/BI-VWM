import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import { createConnection } from 'typeorm'

import Movie from './Movie'
import MovieService from './services/MovieService'
import * as Controller from './controllers/MovieController'

const PORT = 3001

const app = express()

app.use(bodyParser.json())
app.use(cors())

// Connects to database and sets routes for movie requests
createConnection({
	type: 'sqlite',
	database: '../database.db',
	entities: [ Movie ]
}).then(async connection => {
	const service = new MovieService(connection)

	app.post('/movies/sequential', Controller.getSequential(service))
	app.post('/movies/threshold', Controller.getThreshold(service))

	app.listen(PORT, () => console.log('Server listening on port ' + PORT))
}).catch(error => console.log(error))
