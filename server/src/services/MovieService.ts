import { Connection, Repository } from 'typeorm'
import * as R from 'ramda'

import Movie from '../Movie'

export interface MovieData {
	[key: string]: Movie[]
}

export interface AttributeMap {
	[key: string]: number
}

const RELOAD_TIME = 5 * 60 * 1000

/**
 * Manages data loading from database
 * 
 * @export
 * @class MovieService
 * @param {string[]} attributes Attributes that can be searched by
 * @param {MovieData} moviesByAttributes Map of ordered attributes, is indexed by attribute name
 * @param {MovieData} moviesByAttributesReversed Map of ordered attributes(ascending),is indexed by attribute name
 * @param {Movie[]} movies List of all movies
 * @param {AttributeMap} maxAttribute Map of maximum values that is assigned to attribute in database
 * @param {AttributeMap} minAttributeValue Map of absolute values that attribute from database can be below 0
 */
export default class MovieService {
	repository: Repository<Movie>
	attributes: string[]
	moviesByAttributes: MovieData = {}
	moviesByAttributesReversed: MovieData = {}
	movies: Movie[] = []
	maxAttribute: AttributeMap = {}
	minAttributeValue: AttributeMap = {}
	loaded: boolean = false

	/**
	 * Creates an instance of MovieService.
	 * Selects attributes that can be searched by and runs load cycle from database
	 * 
	 * @param {Connection} connection 
	 * @memberof MovieService
	 */
	constructor(connection: Connection) {
		this.repository = connection.getRepository(Movie)

		this.attributes = [
			'worldGross',
			'domesticGross',
			'internationalGross',
			'openingWeek',
			'budget',
			'netIncome',
			'score',
			'screenTime'
		]

		this.reloadData()
	}

	/**
	 * Loads data form database every RELOAD_TIME
	 * 
	 * @memberof MovieService
	 */
	async reloadData() {
		this.loadData()

		setTimeout(() => { this.reloadData() }, RELOAD_TIME)
	}

	/**
	 * Loads all data from database
	 * 
	 * @memberof MovieService
	 */
	async loadData() {
		for (const attribute of this.attributes) {
			try {
				this.moviesByAttributes[attribute] = await this.repository.find({
					order: { [attribute]: 'DESC' }
				})
				this.moviesByAttributesReversed[attribute] = R.reverse(
					this.moviesByAttributes[attribute]
				)
				console.log('Loaded movies by attribute: ' + attribute)
			} catch (error) {
				console.log(error)
			}
		}	

		for (const attribute of this.attributes) {
			try {
				this.minAttributeValue[attribute] = (await this.repository
					.createQueryBuilder('movie')
					.select(`MIN(movie.${attribute})`, 'min')
					.getRawOne() as { min: number })['min']
			} catch (error) {
				console.log(error)
			}

			try {
				this.maxAttribute[attribute] = (await this.repository
					.createQueryBuilder('movie')
					.select(`MAX(movie.${attribute})`, 'max')
					.getRawOne() as { max: number })['max']
			} catch (error) {
				console.log(error)
			}

			if (this.minAttributeValue[attribute] > 0) {
				this.minAttributeValue[attribute] = 0
			} else {
				this.minAttributeValue[attribute] =
					Math.abs(this.minAttributeValue[attribute])
			}

			this.maxAttribute[attribute] += this.minAttributeValue[attribute]
		}

		try {
			this.movies = await this.repository.find()
			console.log('Loaded movies')
		} catch (error) {
			console.log(error)
		}

		this.loaded = true
	}
}
