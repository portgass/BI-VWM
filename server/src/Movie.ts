import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

/**
 * Model of movie table from database
 * 
 * @export
 * @class Movie
 */
@Entity({ name: 'movie' })
export default class Movie {
	@PrimaryGeneratedColumn({ type: 'int' })
	id: number

	@Column({ type: 'varchar' })
	name: string

	@Column({ type: 'varchar' })
	description: string

	@Column({ type: 'varchar' })
	genre: string

	@Column({ name: 'origin_country', type: 'varchar' })
	country: string

	@Column({ type: 'int' })
	year: number

	@Column({ type: 'varchar' })
	poster: string

	@Column({ name: 'length', type: 'int' })
	screenTime: number

	@Column({ type: 'int' })
	score: number

	@Column({ name: 'world_gross', type: 'int' })
	worldGross: number

	@Column({ name: 'domestic_gross', type: 'int' })
	domesticGross: number

	@Column({ name: 'international_gross', type: 'int' })
	internationalGross: number

	@Column({ name: 'opening_week', type: 'int' })
	openingWeek: number

	@Column({ type: 'int' })
	budget: number

	@Column({ name: 'net_income', type: 'int' })
	netIncome: number

	[key: string]: any
	aggregate?: number
}