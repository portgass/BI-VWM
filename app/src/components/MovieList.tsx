import * as React from 'react'

import Movie from '../models/Movie'

import { withStyles, WithStyles } from 'material-ui/styles'
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table'
import Typography from 'material-ui/Typography'

interface Props {
	movies: Movie[]
}

type ClassKeys = 'root' | 'row' | 'cell'

const decorate = withStyles<ClassKeys>(() => ({
	root: {
		overflowY: 'scroll',
		height: '100%'
	},
	row: {
		height: 36,
		'&:last-child > *': {
			borderBottom: 0
		}
	},
	cell: {
		paddingLeft: 8,
		paddingRight: 8,
		'&:first-child': {
			paddingLeft: 24
		},
		'&:last-child': {
			paddingRight: 24
		}
	}
}))

/**
 * Table of result movies
 * 
 * @class MovieList
 * @extends {(React.Component<Props & WithStyles<ClassKeys>>)}
 */
class MovieList extends React.Component<Props & WithStyles<ClassKeys>> {
	render() {
		const { movies, classes } = this.props

		if (!movies || !movies.length) {
			return this.renderNoContent()
		}

		return(
			<div className={classes.root}>
				<Table>
					<TableHead>
						<TableRow className={classes.row}>
							<TableCell className={classes.cell}>
								Name
							</TableCell>
							<TableCell className={classes.cell} numeric={true}>
								World gross
							</TableCell>
							<TableCell className={classes.cell} numeric={true}>
								Domestic gross
							</TableCell>
							<TableCell className={classes.cell} numeric={true}>
								International gross
							</TableCell>
							<TableCell className={classes.cell} numeric={true}>
								Opening week
							</TableCell>
							<TableCell className={classes.cell} numeric={true}>
								Budget
							</TableCell>
							<TableCell className={classes.cell} numeric={true}>
								Net income
							</TableCell>
							<TableCell className={classes.cell} numeric={true}>
								Screen time
							</TableCell>
							<TableCell className={classes.cell} numeric={true}>
								Score
							</TableCell>
							<TableCell className={classes.cell} numeric={true}>
								Aggregate
							</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{movies.map(this.renderMovie)}
					</TableBody>
				</Table>
			</div>
		)
	}

	renderNoContent = () => (
		<Typography>No movies found</Typography>
	)

	renderMovie = (movie: Movie) => (
		<TableRow key={movie.id} className={this.props.classes.row}>
			<TableCell
				className={this.props.classes.cell}
			>
				{movie.name}
			</TableCell>
			<TableCell
				className={this.props.classes.cell}
				numeric={true}
			>
				$&nbsp;{movie.worldGross.toLocaleString()}
			</TableCell>
			<TableCell
				className={this.props.classes.cell}
				numeric={true}
			>
				$&nbsp;{movie.domesticGross.toLocaleString()}
			</TableCell>
			<TableCell
				className={this.props.classes.cell}
				numeric={true}
			>
				$&nbsp;{movie.internationalGross.toLocaleString()}
			</TableCell>
			<TableCell
				className={this.props.classes.cell}
				numeric={true}
			>
				$&nbsp;{movie.openingWeek.toLocaleString()}
			</TableCell>
			<TableCell
				className={this.props.classes.cell}
				numeric={true}
			>
				$&nbsp;{movie.budget.toLocaleString()}
			</TableCell>
			<TableCell
				className={this.props.classes.cell}
				numeric={true}
			>
				$&nbsp;{movie.netIncome.toLocaleString()}
			</TableCell>
			<TableCell
				className={this.props.classes.cell}
				numeric={true}
			>
				{movie.screenTime}&nbsp;minutes
			</TableCell>
			<TableCell
				className={this.props.classes.cell}
				numeric={true}
			>
				{movie.score}/100
			</TableCell>
			<TableCell
				className={this.props.classes.cell}
				numeric={true}
			>
				{movie.aggregate.toLocaleString(undefined, { minimumFractionDigits: 7})}
			</TableCell>
		</TableRow>
	)
}

export default decorate(MovieList)
