import * as React from 'react'

import { withStyles, WithStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'
import Checkbox from 'material-ui/Checkbox'
import {
	FormGroup,
	FormControl,
	FormLabel,
	FormControlLabel
} from 'material-ui/Form'
import { InputLabel } from 'material-ui/Input'
import { MenuItem } from 'material-ui/Menu'
import Radio, { RadioGroup } from 'material-ui/Radio'
import Select from 'material-ui/Select'
import TextField from 'material-ui/TextField'

import { Request } from './App'

const attributeTypes = [
	'worldGross',
	'domesticGross',
	'internationalGross',
	'openingWeek',
	'budget',
	'netIncome',
	'screenTime',
	'score'
]

const capitalize = (text: string) => {
	const capital = text.charAt(0).toUpperCase()
	text = capital + text.slice(1)

	return text
}

interface Props {
	getData: (props: Request) => void
}

interface State {
	aggregate: string
	count: number
	attributes: string[]
	order: 'desc' | 'asc'
}

type ClassKeys = 'root' | 'label' | 'select'

const decorate = withStyles<ClassKeys>(() => ({
	root: {
		display: 'flex',
		flexDirection: 'column',
		marginRight: 32,
		'& > *': {
			marginBottom: 24
		}
	},
	label: {
		marginBottom: 8
	},
	select: {
		height: 24
	}
}))

/**
 * Manages all request settings and user input
 * 
 * @class Search
 * @extends {(React.Component<Props & WithStyles<ClassKeys>, State>)}
 */
class Search extends React.Component<Props & WithStyles<ClassKeys>, State> {
	state: State = {
		aggregate: 'max',
		count: 25,
		attributes: [],
		order: 'desc'
	}

	handleAggregateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({ aggregate: event.target.value })
	}

	handleCountChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({ count: parseInt(event.target.value, 10) })
	}

	handleOrderChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({ order: event.target.value as 'desc' | 'asc' })
	}

	handleAttributeSelect = (attribute: string) => {
		if (!this.state.attributes.includes(attribute)) {
			this.setState({ attributes: [ ...this.state.attributes, attribute ]})
		} else {
			this.setState({ 
				attributes: this.state.attributes.filter(a => a !== attribute)
			})
		}
	}

	render() {
		const { classes, getData } = this.props
		const { aggregate, count, attributes, order } = this.state

		return(
			<div className={classes.root}>
				<FormControl component="fieldset">
					<FormLabel component="legend" className={classes.label}>
						Select attributes
					</FormLabel>
					<FormGroup row={false}>
						{attributeTypes.map(a => (
							<FormControlLabel
								key={a}
								control={this.renderCheckbox(a)}
								label={capitalize(a)}
							/>
						))}
					</FormGroup>
				</FormControl>

				<TextField
					label="Number of results (k)"
					value={count}
					onChange={this.handleCountChange}
					type="number"
					fullWidth={true}
				/>

				<FormControl fullWidth={true}>
					<InputLabel>Aggregate function</InputLabel>
					<Select
						value={aggregate}
						onChange={this.handleAggregateChange}						
					>
						<MenuItem value="max">Max</MenuItem>
						<MenuItem value="min">Min</MenuItem>
						<MenuItem value="average">Average</MenuItem>
						<MenuItem value="sum">Sum</MenuItem>
					</Select>
				</FormControl>

				<FormControl component="fieldset">
					<FormLabel component="legend" className={classes.label}>
						Aggregate order
					</FormLabel>
					<RadioGroup
						value={order}
						onChange={this.handleOrderChange}
					>
						<FormControlLabel
							value="desc"
							control={<Radio />}
							label="More is better"
							className={classes.select}
						/>
						<FormControlLabel
							value="asc"
							control={<Radio />}
							label="Less is better"
							className={classes.select}
						/>
					</RadioGroup>
				</FormControl>

				<Button
					variant="raised"
					color="primary"
					onClick={() => getData({ aggregate, count, attributes, order })}
					disabled={attributes.length === 0}
				>
					Search
				</Button>
			</div>
		)
	}

	renderCheckbox = (attribute: string) => (
		<Checkbox
			className={this.props.classes.select}
			checked={this.state.attributes.includes(attribute)}
			onChange={() => this.handleAttributeSelect(attribute)}
			value={attribute}
		/>
	)
}

export default decorate(Search)
