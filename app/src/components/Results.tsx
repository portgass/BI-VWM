import * as React from 'react'

import { withStyles, WithStyles } from 'material-ui/styles'
import Fade from 'material-ui/transitions/Fade'
import AppBar from 'material-ui/AppBar'
import Button from 'material-ui/Button'
import Divider from 'material-ui/Divider'
import Paper from 'material-ui/Paper'
import Tabs, { Tab } from 'material-ui/Tabs'
import Typography from 'material-ui/Typography'

import MovieList from './MovieList'
import { RequestData } from './App'

interface Props {
	thresholdData: RequestData
	sequentialData: RequestData
	newSearch: boolean
	unsetNewSearch: () => void
}

interface State {
	tabValue: number
	page: number
}

type ClassKeys = 'root' | 'tab' | 'pager' | 'pagerButtons'

const decorate = withStyles<ClassKeys>(() => ({
	root: {
		width: 1200,
		display: 'flex',
		flexDirection: 'column',
		alignSelf: 'stretch'
	},
	tab: {
		minWidth: 500
	},
	pager: {
		minHeight: 48,
		marginLeft: 16,
		marginRight: 16,
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	pagerButtons: {
		'& > *': {
			marginRight: 8
		},
		'&:last-child': {
			marginRight: 0
		}
	}
}))

const PAGE_SIZE = 50

/**
 * Shows result component, tabs of request types, result table and page navigation buttons
 * 
 * @class Results
 * @extends {(React.Component<Props & WithStyles<ClassKeys>>)}
 */
class Results extends React.Component<Props & WithStyles<ClassKeys>> {
	state: State = {
		tabValue: 0,
		page: 0
	}

	static getDerivedStateFromProps(nextProps: Props, prevState: State) {
		if (nextProps.newSearch) {
			nextProps.unsetNewSearch()
		}

		return {
			...prevState,
			page: 0
		}
	}

	handleTabChange = (event: React.ChangeEvent<HTMLDivElement>, value: number) => {
		this.setState({ tabValue: value })
	}

	render() {
		const {
			classes,
			thresholdData,
			sequentialData
		} = this.props
		const { tabValue, page } = this.state

		if (thresholdData.movies.length === 0 || sequentialData.movies.length === 0) {
			return null
		}

		const tLabel = 
			`Threshold results (${thresholdData.time.toLocaleString()} ms, ${thresholdData.count} iterations)`
		const sLabel = 
			`Sequential results (${sequentialData.time.toLocaleString()} ms, ${sequentialData.count} iterations)`

		const thresholdMovies = thresholdData.movies.slice(PAGE_SIZE * page, PAGE_SIZE * page + PAGE_SIZE)
		const sequentialMovies = sequentialData.movies.slice(PAGE_SIZE * page, PAGE_SIZE * page + PAGE_SIZE)
		const pageCount = Math.ceil(thresholdData.movies.length / PAGE_SIZE)

		return(
			<Fade in={true}>
				<Paper className={classes.root}>
					<AppBar position="static" color="default">
						<Tabs
							value={tabValue}
							onChange={this.handleTabChange}
							indicatorColor="primary"
							textColor="primary"
							fullWidth={true}
							centered={true}
						>
							<Tab label={tLabel} className={classes.tab} />
							<Tab label={sLabel} className={classes.tab} />
						</Tabs>
					</AppBar>

					{tabValue === 0 && <MovieList movies={thresholdMovies} />}
					{tabValue === 1 && <MovieList movies={sequentialMovies} />}

					<Divider />

					<div className={classes.pager}>
						<Typography variant="subheading">
							Total pages: {pageCount}
						</Typography>
						<div className={classes.pagerButtons}>
							<Button
								onClick={() => this.setState({ page: page - 1 })}
								disabled={page === 0}
							>
								Previous {PAGE_SIZE}
							</Button>
							<Button
								onClick={() => this.setState({ page: page + 1 })}
								disabled={thresholdData.movies.length <= (page + 1) * PAGE_SIZE}
							>
								Next {PAGE_SIZE}
							</Button>
						</div>
					</div>
				</Paper>
			</Fade>
		)
	}
}

export default decorate(Results)
