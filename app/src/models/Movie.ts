export default class Movie {
	id: number
	name: string
	description: string
	genre: string
	year: number
	country: string
	worldGross: number
	domesticGross: number
	internationalGross: number
	openingWeek: number
	netIncome: number
	budget: number
	screenTime: number
	score: number
	poster: string
	aggregate: number
}