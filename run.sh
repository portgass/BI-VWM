#!/bin/bash

_termhandle() {
	kill $PIDAPI $PIDAPP
}

trap _termhandle SIGTERM SIGINT

cd server
yarn install
yarn start &
PIDAPI=$!

cd ../app
yarn install
yarn start &
PIDAPP=$!

echo "Server and application running"
wait $PIDAPI $PIDAPP
